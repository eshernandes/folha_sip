﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOLHA_API_SIP
{
    public interface IRH
    {
        void Imprimir();
        void AddFuncionario(IFuncionario f);
        void AddEstagiario(IEstagiario e);
    }
}
