﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOLHA_API_SIP
{
    public class Estagiario : IEstagiario
    {
        public long Cod { get; set; }
        public string Nome { get; set; }
        public double SB { get; set; }
        public double SL { get; set; }
        public double Bonus { get; set; }

        public Estagiario(long Cod, string Nome, double SB)
        {
            this.Cod = Cod;
            this.Nome = Nome;
            this.SB = SB;
        }

        public void Ponto(double horas, double salHora)
        {
            SB = horas * salHora;
            Bonus = SB * 0.02;

            Console.WriteLine("Horas: {0} \tSalário Hora: {1}",
                horas, salHora);
        }

        public void Calcular()
        {
            Console.WriteLine("BÔNUS: {0:C2}", Bonus);

            SL = SB + Bonus;
            Console.WriteLine("Salário Líquido: {0:C2}", SL);
        }

        public void Folha()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("COD: {0}\tNome: {1}", Cod, Nome);
            Console.WriteLine("Salário Bruto: {0:C2}", SB);
            Console.ForegroundColor = ConsoleColor.Blue;
            Calcular();
        }

        public double INSS()
        {
            throw new NotImplementedException();
        }

        public double IRRF()
        {
            throw new NotImplementedException();
        }
    }
}
