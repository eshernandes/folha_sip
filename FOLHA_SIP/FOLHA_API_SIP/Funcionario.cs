﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOLHA_API_SIP
{
    public class Funcionario : IFuncionario
    {
        public long Cod { get; set; }
        public string Nome { get; set; }
        public double SB { get; set; }
        public double SL { get; set; }

        public Funcionario(long Cod, string Nome, double SB)
        {
            this.Cod = Cod;
            this.Nome = Nome;
            this.SB = SB;
        }

        public double INSS()
        {
            if (SB < 1830.29) return SB * 0.08;
            else if (SB >= 1830.29 && SB < 3050.52)
                return SB * 0.09;
            else return SB * 0.11;
        }

        public double IRRF()
        {
            // Tabela 2022
            if (SB < 1903.29) return 0;
            else if (SB >= 1903.29 && SB < 2865.65)
                return SB * 0.075;
            else if (SB >= 2865.65 && SB < 3751.05)
                return SB * 0.15;
            else if (SB >= 3751.05 && SB < 4665.68)
                return SB * 0.225;
            else return SB * 0.27;
        }

        public void Calcular()
        {
            double Previdencia = INSS();
            Console.WriteLine("Previdência: {0:C2}", 
                Previdencia);

            double IR = IRRF();
            Console.WriteLine("IR: {0:C2}", 
                IR);

            double descontos = IR + Previdencia;
            Console.WriteLine("Total de Descontos: {0:C2}",
                descontos);
            SL = SB - descontos;

            Console.WriteLine("Salário Líquido: {0:C2}",
                SL);
        }

        public void Folha()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("COD: {0} \tNome: {1}",
                Cod, Nome);
            Console.WriteLine("Salário Bruto: {0:C2}",
                SB);
            Console.ForegroundColor = ConsoleColor.Blue;
            Calcular();
        }

        public void Ponto(double horas, double salHora)
        {
            SB = horas * salHora;
            Console.WriteLine("Horas: {0} \tSalário Hora: {1}",
                horas, salHora);
        }
    }
}
