﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOLHA_API_SIP
{
    public class RH : IRH
    {
        public ArrayList Empregados { get; set; }
        public RH()
        {
            Empregados = new ArrayList();
        }
        public void Imprimir()
        {
            Random sorteio = new Random();
            foreach (dynamic emp in Empregados)
            {
                emp.Ponto(sorteio.Next(180, 220),
                    sorteio.Next(16, 25));
                emp.Folha();
                Console.WriteLine("\n");
            }
        }

        public void AddFuncionario(IFuncionario f)
        {
            Empregados.Add(f);
        }

        public void AddEstagiario(IEstagiario e)
        {
            Empregados.Add(e);
        }
    }
}
