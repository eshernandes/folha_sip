﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOLHA_API_SIP
{
    public interface IFuncionario : ISalario, IImposto
    {
        void Ponto(double horas, double salHora);
    }
}
