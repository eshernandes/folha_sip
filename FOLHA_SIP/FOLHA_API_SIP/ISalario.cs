﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOLHA_API_SIP
{
    public interface ISalario
    {
        void Calcular();
        void Folha();
    }
}
