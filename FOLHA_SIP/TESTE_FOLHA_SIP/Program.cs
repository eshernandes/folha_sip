﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOLHA_API_SIP;

namespace TESTE_FOLHA_SIP
{
    class Program
    {
        static IRH Empresa;

        static void Main(string[] args)
        {
            Empresa = new RH();

            Empresa.AddFuncionario(new Funcionario(1, "João", 0f));
            Empresa.AddFuncionario(new Funcionario(2, "Maria", 0f));
            Empresa.AddFuncionario(new Funcionario(3, "José", 0f));

            Empresa.AddEstagiario(new Estagiario(4, "Aspira", 0f));
            Empresa.AddEstagiario(new Estagiario(5, "Caio", 0f));
            Empresa.AddEstagiario(new Estagiario(6, "Rhuan", 0f));

            Empresa.Imprimir();
            Console.ReadKey();
        }
    }
}
